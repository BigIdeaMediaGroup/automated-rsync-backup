#!/bin/bash
#
# Site Backup Utility

########################################################################
# Constants
########################################################################

# Environment
NOW=$(date +"%Y-%m-%d")
RECIPIENT="stephen@bigideamediagroup.com"
DIR=$5
BACKUP_DIR="$HOME/.backup"

# Database Credentials
DB_NAME=$1
DB_USER=$2
DB_PW=$3
DB_HOST=$4

# Files
SQL_FILE="$BACKUP_DIR/${DB_NAME}_$NOW.sql"
SQL_ERR="$BACKUP_DIR/mysqldump.error"
TAR_FILE="$BACKUP_DIR/${DIR}_$NOW.tar.bz2"
TAR_ERR="$BACKUP_DIR/tar.error"

########################################################################
# Functions
########################################################################

run_cleanup () {
  if [ ! -d $BACKUP_DIR ]; then
    echo "The directory $BACKUP_DIR does not exist!"
    exit 1
  elif [ "$( ls -A $BACKUP_DIR )" ]; then
    rm $BACKUP_DIR/*
  else
    echo "No files in $BACKUP_DIR to clean up."
    exit 1
  fi
}

run_backup () {
  if [ ! -e $BACKUP_DIR ]; then
    mkdir $BACKUP_DIR
  fi

  mysqldump --opt \
            --user=$DB_USER \
            --password=$DB_PW \
            --host=$DB_HOST \
            $db_name > $SQL_FILE 2> $SQL_ERR

  if [ -s $SQL_ERR ]; then
    cat $SQL_ERR | mail -s "$DIR backup mysqldump error" $RECIPIENT
    exit 1

  else
	echo "SQL dump successful!"
  fi

  tar cjf $TAR_FILE $DIR 2> $TAR_ERR 

  if [ -s $TAR_ERR ]; then
    cat $TAR_ERR | mail -s "$DIR backup tar error" $RECIPIENT
    exit 1
  
  else "Tarball created successfully!" 

  fi
}

usage () {
  echo "Usage: backup [OPTION]
Backup website files and database.
Run backup when called without options.

Options:
  -b, --backup    backup website files and database
  -c, --cleanup   cleanup old backup files
  -h, --help      show this help text"
}

########################################################################
# Main
########################################################################

if [ $# == 0 ]; then
  run_backup
  exit 0
elif [ $# -gt 6 ]; then
  usage
  exit 1
else
    case $1 in
    -b | --backup )   run_backup
                      exit 0;;
    -c | --cleanup )  run_cleanup
                      exit 0;;
    -h | --help )     usage 
                      exit 0;;                                                                  
#    -d | --dir )      $DIR="${OPTARG}";;
#                      #exit 0;;
#    -n | --name )     $DB_NAME="${OPTARG}";;
#                      #exit 0;;
#    -u | --user )     $DB_USER="${OPTARG}";;
#                      #exit 0;;
#    -p | --password ) $DB_PW="${OPTARG}";;
#                      #exit 0;;
#    --host )          $DB_HOST="${OPTARG}";;
                      #exit 0;;
    * )               run_backup 
                      exit 0
  esac
fi
